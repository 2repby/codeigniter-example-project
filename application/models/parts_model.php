<?php
class Parts_model extends CI_Model {
        public function __construct()
        {
                $this->load->database();
        }
       public function get_part($ID_part)
        {
            $query = $this->db->get_where('parts', array('Part_ID' => $ID_part));
            return $query->row_array();
        }

		public function get_parts_page($limit, $start, $search='') 
        {
			if($search != '') $this->db->like('part_name', $search);
            $this->db->limit($limit, $start);
            $query = $this->db->get("parts",$limit,$start);
            return $query->result_array();
        }

		public function get_total($search = '') 
        {
            $this->db->select('count(*) as allcount');
			$this->db->from('parts');
		    if($search != '') $this->db->like('part_name', $search);
      		$query = $this->db->get();
			$result = $query->result_array();
			return $result[0]['allcount'];
        }

		
		public function create_part()
        {
            $this->load->helper('url');
            $part_name = url_title($this->input->post('part_name'), 'dash', TRUE);
            $material = url_title($this->input->post('material'), 'dash', TRUE);
            $weight = $this->input->post('weight');
            $data = array(
                'Part_name' => $part_name,
                'Material'=> $material,
                'Weight'=> $weight
            );
            return $this->db->insert('parts', $data);
        }
		public function delete_part($ID_part = FALSE)
        {
            $this->load->helper('url');
            if ($ID_part != FALSE)
            {
                
            
            $data = array(
                'Part_ID' => $ID_part,
                
            );
            return $this->db->delete('parts', $data);
            }
        }

}
