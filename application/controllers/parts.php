<?php
class Parts extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('parts_model');
				$this->load->library('pagination'); 
				$this->load->library('session');
        }
		
		// Вывод одной детали
        public function view($ID_part = NULL) 
        {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['parts_item'] = $this->parts_model->get_part($ID_part);
            if (empty($data['parts_item']))
            {
                    show_404();
            }

            $data['title'] = $data['parts_item']['Part_name'];

            $this->load->view('templates/header', $data);
            $this->load->view('parts/view', $data);
            $this->load->view('templates/footer');
        }

		//Вывод заданного числа записей начиная с record
        public function view_page($record = 0)
        {
            //очистка переменной, в которой хранится подстрока поиска
			$search_text = "";
			//если форма для ввода подстроки поиска была заполнена 
			if (!isset($this->input->post('search')->value))
			{
				//чтение post-параметра
				$search_text = $this->input->post('search');
				//установка сессионной переменной
				$this->session->set_userdata(array("search"=>$search_text));
			}
			//если пользователь не вводил значение подстроки поиска 
			else
			{
				//если сессионная переменная устновлена...
				if($this->session->userdata('search') != NULL){
					//...то считать ее значение в переменную
					$search_text = $this->session->userdata('search');
				}
			}
			$rows_on_page = 5; //количество строк на странице по умолчанию
			//если форма для ввода количества строк на странице была заполнена 
			if($this->input->post('perpage') != NULL ) 
			{
				//чтение post-параметра
				$rows_on_page = $this->input->post('perpage');
				//установка сессионной переменной
				$this->session->set_userdata(array("perpage"=>$rows_on_page));
			}
			//если пользователь не выбирал количество строк 
			else
			{
				//если сессионная переменная устновлена...
				if($this->session->userdata('perpage') != NULL){
					//...то считать ее значение в переменную
					$rows_on_page = $this->session->userdata('perpage');
				}
			}
			$config['per_page'] = $rows_on_page;
			$config['base_url'] = '/hmc/parts';
            $config['total_rows'] = $this->parts_model->get_total($search_text);
            $config['attributes'] = array('class' => 'page-link');
			$config['first_link'] = 'В начало';
            $config['first_tag_open'] = '<li class="page-item">';
            $config['first_tag_close'] = '</li>';
            $config['last_link'] = 'В конец';
            $config['last_tag_open'] = '<li class="page-item">';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = '&gt;';
            $config['next_tag_open'] = '<li class="page-item">';
            $config['next_tag_close'] = '</li>';
            $config['prev_link'] = '&lt;';
            $config['prev_tag_open'] = '<li class="page-item">';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link">';
            $config['cur_tag_close'] = '</a></li>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
			
			$this->pagination->initialize($config);
					
			$data['parts'] = $this->parts_model->get_parts_page($config['per_page'],$record,$search_text);
            $data['links'] = $this->pagination->create_links();
            $data['title'] = 'Каталог деталей';
			$data['search'] = $search_text;
			$data['perpage'] = $rows_on_page;
			
			$this->load->view('templates/header', $data);
            $this->load->view('parts/index', $data);
            $this->load->view('templates/footer');
        }

		
/*
        public function index()
        {
            $data['parts'] = $this->parts_model->get_parts();
            $data['title'] = 'Part catalogue';
            $this->load->view('templates/header', $data);
            $this->load->view('parts/index', $data);
            $this->load->view('templates/footer');
        }

        public function view($ID_part = NULL)
        {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['parts_item'] = $this->parts_model->get_parts($ID_part);
            if (empty($data['parts_item']))
            {
                    show_404();
            }

            $data['title'] = $data['parts_item']['Part_name'];

            $this->load->view('templates/header', $data);
            $this->load->view('parts/view', $data);
            $this->load->view('templates/footer');
        }*/
		public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $data['title'] = 'Create a parts item';
            $this->form_validation->set_rules('part_name', 'Part_name', 'required');
            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('parts/create');
                $this->load->view('templates/footer');
            }
            else
            {
                $this->parts_model->create_part();
                $this->load->view('parts/success');
            }
        }
		public function delete_part($ID_part = NULL)
        {
            $this->load->helper('form');
            $data['title'] = 'Delete a parts item';
            $data['part_id'] = $ID_part;
            
            {
                $this->parts_model->delete_part($ID_part);
                $this->load->view('parts/success');
            }
            
        }

		
}
