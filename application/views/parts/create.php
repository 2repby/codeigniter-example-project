<h2><?php echo $title; ?></h2>

<?php echo validation_errors(); ?>

<?php echo form_open('parts/create'); ?>

    <label for="part_name">Наименование</label>
    <input type="text" name="part_name" /><br />
    <label for="material">Материал</label>
    <input type="text" name="material" /><br />
    <label for="weight">Вес</label>
    <input type="text" name="weight" /><br />
    <input type="submit" name="submit" value="Create new part" />

</form>
