<div class="container">
	<div class="row">
		<div class="col">
			<ul class="pagination justify-content-end">
				<?php if (isset($links))  echo $links; ?>
			</ul>
		</div>
		<div class="col">
			<form class="form-inline" method='post' action="/hmc/parts/">
				<select name="perpage" class="custom-select col-2">
					<option value="3" <?php if ($perpage=='3') echo ("selected") ?> >3</option>
					<option value="5" <?php if ($perpage=='5') echo ("selected") ?> >5</option>
					<option value="10" <?php if ($perpage=='10') echo ("selected") ?> >10</option>
					<option value="20" <?php if ($perpage=='20') echo ("selected") ?> >20</option>
					<option value="50" <?php if ($perpage=='50') echo ("selected") ?> >50</option>
				</select>
				<div class="input-group-append">
					<input class="btn btn-outline-secondary" type="submit" name='submit' value="На странице">
				</div>
			</form>
		</div>
    </div>
</div>

<form class="form-inline my-2 my-lg-0" method='post' action="/hmc/parts/">
     <input class="form-control mr-sm-2" type="search" placeholder="Введите текст" aria-label="Search" name="search" value='<?php echo $search ?>'>
	 <input class="btn btn-outline-success my-2 my-sm-0" type="submit" name='submit' value="Найти"/>
</form>
<br>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">ID детали</th>
      <th scope="col">Наименование</th>
      <th scope="col">Материал</th>
      <th scope="col">Вес</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>

<?php foreach ($parts as $parts_item): ?>
    <tr>
      <td><?php echo $parts_item['Part_ID'] ?></td>
      <td><?php echo $parts_item['Part_name'] ?></td>
      <td><?php echo $parts_item['Material'] ?></td>
      <td><?php echo $parts_item['Weight'] ?></td>
      <td><a href="/hmc/parts/view/<?php echo $parts_item['Part_ID'] ?>">Просмотреть</a></td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
